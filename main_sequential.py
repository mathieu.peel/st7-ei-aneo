import random as rd
import json
import numpy as np
import process_DAG
import time_module
import time
import argparse
import sys
from pathlib import Path
import graph_separate

from deap import base
from deap import creator
from deap import tools
from Analyser import Parser
from deap_chromosome_sequential import *
from mpi4py import MPI
import mpi4py.futures 
import process_DAG
from Analyser import Parser
import matplotlib.pyplot as plt


MPI_COMM=MPI.COMM_WORLD
MPI_SIZE=MPI_COMM.Get_size()
MPI_RANK=MPI_COMM.Get_rank()


class Flags:
    regressor = None
    euro = False
    verboseMode = False
    seed = None
    loops = 1
    infile = None
    indir = None
    fdout = None
    processes = None

    def __init__(self, argparser):
        args = argparser.parse_args()
        if not args.infile and not args.indir:
            print('Missing parameter --infile or --indir')
            parser.print_help()
            sys.exit(1)
        elif args.infile:
            self.infile = args.infile
        elif args.indir:
            self.indir = args.indir
        if args.verbose:
            self.verboseMode = True
        if args.loops:
            self.loops = args.loops
        if args.processes:
            self.processes = args.processes
        if args.seed:
            self.seed = args.seed
        if args.outfile:
            self.fdout = open(args.outfile, 'w')

class ArgsWrapper:
    def __init__(self, infile, iLoop, verbose):
        self.infile = infile
        self.iLoop = iLoop
        #self.rng = rng
        self.verbose = verbose

def runGenetic(infile, flags: Flags):
    for i in range(0, flags.loops):
        args =ArgsWrapper(infile, i, flags.verboseMode)
        p = Parser(args.infile)
        
        if MPI_RANK==0:
            print("open the graph")
            sys.stdout.flush()

            graphpath=p.get("Graph path")
            with open(graphpath) as json_file:
                data=json.load(json_file)
            full_graph=data['nodes']
            graph=process_DAG.init_Graph(full_graph)

            if p.get('reduction')==1:
                graph= process_DAG.graph_rewrite_from_reduction(graph)
                print("reducing the graph")
                sys.stdout.flush()


            #We want to branch out the graph
            print("separate the graph")
            topo_sort=process_DAG.RandomTopologicalOrder(graph)
            graphs=graph_separate.graph_slicer_main(graph,topo_sort,MPI_SIZE) 
            local_graph=graphs[0]
            # print(local_graph)
            
            print("push to each branch")
            for i in range(1,MPI_SIZE):
                MPI_COMM.send(graphs[i],i)

        else:
            local_graph=MPI_COMM.recv(source=0)
            # print(MPI_RANK)
            # print(local_graph)

        if MPI_RANK==0:
            print("initialize the deap algorithm")
            sys.stdout.flush()

        #now each one has a local graph : (graph, rank, ft_in, ft_out)
        Gene = Genetic(p, local_graph[0], local_graph[2], local_graph[3], MPI_RANK, args.verbose)
        if args.verbose:
            print("verbose mode = true")
            sys.stdout.flush()
        toolbox = base.Toolbox()
        Gene.init_deap(toolbox)
        Gene.update_gene(1)
        local_max_list=[]
        local_min_list=[]
        local_avg_list=[]
        max_list_add,pop,FT_next,min_list_add,avg_list_add=Gene.fit(toolbox)
        
        for i in max_list_add:
            local_max_list.append(i)

        for i in min_list_add:
            local_min_list.append(i)

        for i in avg_list_add:
            local_avg_list.append(i)


        print('FT next : {}, rank : {}'.format(FT_next,MPI_RANK))
        sys.stdout.flush()

        for j in range(10):
            FT_tasks=MPI_COMM.gather(FT_next,root=0)
            # print(FT_tasks)
            if MPI_RANK==0:
                FT=time_module.time(0,0,0,0)
                for i in range(MPI_SIZE):
                    FT.add(FT_tasks[i])
                print("{} cumulated best time :  \t\t\t {}, \t so \t {}".format(j, FT, FT.to_float()))
                sys.stdout.flush()

            # Gene.update_FT(pop,toolbox,FT)
            Gene.update_gene(10)
            max_list_add,pop,FT_next,min_list_add,avg_list_add=Gene.fit(toolbox, u_pop=pop,keep_pop=True)

            for i in max_list_add:
                local_max_list.append(i) 
                
            for i in min_list_add:
                local_min_list.append(i)

            for i in avg_list_add:
                local_avg_list.append(i)       

        #recuperate the max_list:
        max_list_tmp=MPI_COMM.gather(local_max_list,root=0)
        min_list_tmp=MPI_COMM.gather(local_min_list,root=0)
        avg_list_tmp=MPI_COMM.gather(local_avg_list,root=0)


        if MPI_RANK==0:

            #reduce
            max_list=[sum([max_list_tmp[j][i] for j in range(MPI_SIZE)]) for i in range(len(max_list_tmp[0]))]
            avg_list=[sum([avg_list_tmp[j][i] for j in range(MPI_SIZE)]) for i in range(len(avg_list_tmp[0]))]
            min_list=[sum([min_list_tmp[j][i] for j in range(MPI_SIZE)]) for i in range(len(min_list_tmp[0]))]
            

            fig= plt.figure()
            heights=[5,1]
            grid=plt.GridSpec(2,3,wspace=0.25,hspace=0.3,height_ratios=heights)
            ax1=fig.add_subplot(grid[0,0])
            ax2=fig.add_subplot(grid[0,1])
            ax3=fig.add_subplot(grid[0,2])
            ax4=fig.add_subplot(grid[1,:])

            ngene=len(max_list)

            ax1.plot([i+1 for i in range(ngene)], min_list, label="min")
            ax1.plot([i+1 for i in range(ngene)], avg_list, label="avg")
            ax1.plot([i+1 for i in range(ngene)], max_list, label="max")
            ax1.fill_between([i+1 for i in range(ngene)], min_list,max_list, alpha=0.2, color="black")
            ax1.legend()
            ax1.set_xticks([i+1 for i in range(ngene)],[i+1 for i in range(1,ngene,10)])
            ax1.set_title("Min, average, and max time for each generation",pad=20)
            ax1.set(xlabel="generations",ylabel="time")
            ax1.ticklabel_format(style="sci",axis="y", scilimits=(0,0))
            ax1.grid(True)
            
            fig.set_size_inches(15,10)
            plt.savefig("./output/PLOT_parallel.pdf")
            plt.close()

            

    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    group1 = parser.add_mutually_exclusive_group(required=True)
    group1.add_argument("--infile", help="Path to the files describing the problems. One file per problem", nargs='+', type=str)
    group1.add_argument("--indir", help="Run all problems inside the directory.", type=str)
    parser.add_argument("--outfile", "-o", help="Path to the output file", type=str)
    parser.add_argument("--verbose", help="Verbose", action="store_true")
    parser.add_argument("--seed", help="Rng seed", type=int)
    parser.add_argument("--loops", help="Number of loops", type=int)
    parser.add_argument("--processes", help="Number of processes to run in parallel when using --loops", type=int)

    argflags = Flags(parser)
    Files = []
    if argflags.infile is not None:
        Files = [f for f in argflags.infile if f.endswith('.txt') and not f.endswith('_results.txt')]

    if argflags.indir is not None:
        Files = [f for f in Path(argflags.indir).iterdir() if f.match('*.txt') and not f.match('*_results.txt')  and not f.endswith('.template.txt')]

    nFiles = len(Files)
    for idx, f in enumerate(Files):
        runGenetic(f, argflags)

    if argflags.fdout is not None:
        argflags.fdout.close()