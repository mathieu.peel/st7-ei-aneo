import json
import random as rd
import networkx as nx



def which_rank_node(node,topo_slice):
    rank=0
    not_found=True
    while not_found:
        if node in topo_slice[rank]:
            not_found=False
            return rank
        rank+=1

#Then slice the sort
def graph_slicer(topo_slice,networkx_graph,rank_slice):
    #need for a new graph, constrained to the topological nodes,
    #but with additionnal information on potential precedent nodes
    #on potential outbound nodes
    #and from which part of the graph to another part
    #with time information, ST, FT, that will be updated

    #To get quicker, we also save a dictionnary to point to what node to update to change
    #each change of FT for the previous graph
    #The keys of FT_changes give us every incoming node 
    G=nx.DiGraph()
    nodes_int={}
    FT_in={}
    FT_out={}
    rank_dict={}
    for i in topo_slice[rank_slice]:
        nodes_int[i]=i
    
    for node in topo_slice[rank_slice]:
        in_dependence={}
        #add node
        G.add_node(node)
        G.nodes[node]["time"]=networkx_graph.nodes[node]['time']

        #dependencies
        for dependence in networkx_graph.predecessors(node):
            if dependence in nodes_int:
                G.add_edges_from([(dependence,node)])
            else:
                #we still need some info on this node
                #-FT
                #-rank in topo_slice
                rank=which_rank_node(dependence,topo_slice)
                in_dependence[dependence]={"FT":networkx_graph.nodes[dependence]["time"]}
                
                if dependence not in FT_in:
                    FT_in[dependence]=[(node,rank)]
                else:
                    FT_in[dependence] = FT_in[dependence] + [(node,rank)]
                
        G.nodes[node]["in_dep"]=in_dependence.copy()
        
        #outbound nodes coming into the next graphs
        for outbound_node in networkx_graph.neighbors(node):
            if outbound_node not in nodes_int:
                #an outbound node
                rank=which_rank_node(outbound_node,topo_slice)
                if node not in FT_out:
                    FT_out[node]=[(outbound_node,rank)]
                else:
                    FT_out[node]=FT_out[node] + [(outbound_node,rank)]
    return G, rank_slice, FT_in, FT_out

def graph_slicer_main(networkx_graph,topo_sort,n_slices):
    graph_partition=[]
    N=len(topo_sort)
    slicing=[i*int(N/n_slices) for i in range(n_slices+1)]
    slicing[-1]=N
    topo_sliced=[topo_sort[slicing[i]:slicing[i+1]] for i in range(n_slices)]
    for rank in range(n_slices):
        graph_partition.append(graph_slicer(topo_sliced,networkx_graph, rank))
    return graph_partition


def update_graph_local(local_graph,FT_in,FT_changes):
    #FT_changes : dictionnaires avec FT_chanes[noeud]= FT de ce noeud
    for in_node in FT_changes:
        if in_node in FT_in:
            for (corr_node,corr_rank) in FT_in[in_node]:
                local_graph.nodes[corr_node]["in_dep"][in_node]['FT']=FT_changes[in_node]
    pass


# graphs=[
# "easy_graph.json",
# "P4L5.json",
# "smallRandom.json",
# "xsmallComplex.json",
# "smallComplex.json",
# "mediumRandom.json",
# "MediumComplex.json",
# "largeComplex.json",
# "xlargeComplex.json",
# "xxlargeComplex.json"
# ]

# local = "../Graphs/"

# import process_DAG

# g=process_DAG.init_Graph(process_DAG.path_to_full_graph(filepath))
# topo_sort=process_DAG.RandomTopologicalOrder(g)
# graphs=graph_slicer_main(g,topo_sort,size)
#technically not parallelised but can be if we transfer all the data (each processor slices its part)

#We end up with n_slices partial graphs with the dependencies coming from
#the previous graph in the topological order
#But we also have to know what FT the next graph needs in order to update them
#The usable type is a quadruplet (sub_graph, rank, FT_in, FT_out)

# sliced_graph=graphs[2][0]
# FT_changes_sg=graphs[2][1]
# FT_prec={'2':time_module.time(0,0,1,0),'5':time_module.time(0,0,2,0)}

# update_graph_FT(sliced_graph,FT_changes_sg,FT_prec)

# from networkx.viewer import viewer

# app=Viewer(sliced_graph)
# app.mainloop()

# local_graph=graphs[1][0]
# FT_in = graphs[1][2]
# FT_changes={'1':time_module.time(0,0,100,0),'2':time_module.time(0,0,200,0)}
# update_graph_local(local_graph,FT_in,FT_changes)

#We want to call, for each sub graph, a deap runtime, but with a specific graph and updated evaluation function
#It should return each 10-15 generations, the FT of FT_out nodes
#We have to get all of these FT_out, then calculate the new FT_changes to send to each subproblem