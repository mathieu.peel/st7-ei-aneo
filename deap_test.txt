##ceci est un commentaire volatility = 0.2

Graph path <string> Graphs/largeComplex.json
model type <string> bs
Population size <int> 40
Selection size <int> 40
Weights <float> -1.0
cpga probability <float> 0.8
crossover probability <float> 0.5
mutation probability <float> 0.2
mutate order <float> 0.0
mutate order each n generations <int> 75
reduction <int> 0
plot <int> 0
save stats <int> 0

Number of processors <int> 4
Number of generations <int> 10
