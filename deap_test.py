import random as rd
import json
import numpy as np
import process_DAG
import time_module
import multiprocessing
import time
import mpi4py.futures

from deap import base
from deap import creator
from deap import tools

from process_DAG import init_Graph, RandomTopologicalOrder

M = 1
m = M
opened_graph = "Graphs/smallComplex.json" #"Graphs/easy_graph.json"
path = "Graphs/smallComplex.json" #"Graphs/easy_graph.json"
with open(opened_graph) as json_file:
    data = json.load(json_file)
full_graph = data["nodes"]
graph = process_DAG.init_Graph(full_graph)

def init_individual():
    with open(path) as json_file:
        data = json.load(json_file)

    full_graph = data["nodes"]
    G=init_Graph(full_graph)
    return np.array([RandomTopologicalOrder(G), [rd.randint(1, M) for _ in range(G.number_of_nodes())]])

#def init_population

def crossover_map(ind1,ind2):
    n=len(ind1[0])
    crossover_point=rd.randint(0,n-1)
    save=np.copy(ind1[1,crossover_point:])
    ind1[1,crossover_point:]=np.copy(ind2[1,crossover_point:])
    ind2[1,crossover_point:]=save

def crossover_cpga(ind1,ind2): #Conserve dans chaque individu son début
    n=len(ind1[0])
    crossover_point=rd.randint(0,n-1)
    ind1_original,ind2_original=np.copy(ind1),np.copy(ind2)
    ind1_c,ind2_c=np.copy(ind1[:,crossover_point:]),np.copy(ind2[:,crossover_point:])

    ind1_c_0=ind1_c[0,:]
    ind1_c_0.flatten()
    ind2_c_0=ind2_c[0,:]
    ind2_c_0.flatten()

    ind1_0=np.copy(ind1[0,:])
    ind1_0.flatten()
    ind2_0=np.copy(ind2[0,:])
    ind2_0.flatten()

    l_index_2,l_index_1=[],[]
    for i,j in zip(ind1_c_0,ind2_c_0):
        l_index_2.append(np.where(ind2_0==i)[0][0])
        l_index_1.append(np.where(ind1_0==j)[0][0])
    l_index_1.sort()
    l_index_2.sort()
    
    ind1[:,crossover_point:]=ind2_original[:,l_index_2]
    ind2[:,crossover_point:]=ind1_original[:,l_index_1]

def evaluate(individual):
    # implement the schedule length function as seen in page 4 of the paper
    #m Nb of processors
    NbNodes = len(individual[0])

    RT = [time_module.time(0, 0, 0, 0) for _ in range(m)]
    processors = list(individual[1]).copy()
    LT = list(individual[0]).copy()
    ST = {}
    FT = {}

    for i in range(NbNodes):
        task = LT.pop(0)

        Pj = int(processors[i])
        predecessors = list(graph.predecessors(task))
        DAT = time_module.time(0, 0, 0, 0)
        for dependency in predecessors:
            if FT[str(dependency)] >= DAT:
                DAT = FT[str(dependency)].copy()

        #print("DAT", DAT)
        #print("RT", Pj -1)
        if DAT >= RT[Pj - 1]:
            ST[task] = DAT.copy()
        else:
            ST[task] = RT[Pj - 1].copy()

        FT[task] = time_module.add(ST[task], graph.nodes[task]["time"])
        RT[Pj - 1] = FT[task].copy()

    Final_time = time_module.time(0, 0, 0, 0)
    for i in FT.keys():
        if FT[i] >= Final_time:
            Final_time = FT[i]
    #print("FT",Final_time)
    #print("REPE", Final_time.to_float())
    return (Final_time.to_float(),)

def mutation(arg):#(ind,prob)
    ind,prob=arg
    n=len(ind[0])
    for i in range(n):
        if rd.random()<prob:
            random_processor=rd.randint(1,M)
            ind[1][i]=random_processor
            del ind.fitness.values

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", np.ndarray, fitness=creator.FitnessMin)
toolbox = base.Toolbox()
toolbox.register("init_ind", init_individual)
toolbox.register("individual", tools.initIterate, creator.Individual)
toolbox.register("population",tools.initRepeat, list, toolbox.individual)
toolbox.register("cmap",crossover_map)
toolbox.register("cpga",crossover_cpga)
toolbox.register("evaluate",evaluate)
toolbox.register("select",tools.selTournament,tournsize=3)
toolbox.register("mutate",mutation)
#crossover_cpga(ind1,ind2)
#print(ind1,ind2)

'''
ind1 = toolbox.individual()s
ind2 = toolbox.individual()
print(ind1, ind2)
toolbox.register("cmap", crossover_map)

toolbox.cmap(ind1,ind2)
print(ind1, ind2)

ind1.fitness.values = evaluate(ind1)
print(ind1.fitness.valid)
print(ind1.fitness)
'''

def main():
    pool = mpi4py.futures.MPIPoolExecutor(max_workers=6)
    #pool=multiprocessing.Pool()
    toolbox.register("map",pool.map)
    print('Using ' + str(multiprocessing.cpu_count()) + ' processors.')
    number_generations=5
    mutation_probability=.05
    pop_size=5
    select_size=2

    # Define probability of each crossover conditioned by "We accept to make a crossover"
    cpga_probability=.5
    cmap_probability=1-cpga_probability

    # Define the probability to accept to make a crossover
    crossover_probability=.7

    # Initiate the population
    #pop = toolbox.population(n=pop_size)
    pop=list(toolbox.map(toolbox.individual,[toolbox.init_ind for _ in range(pop_size)]))
    g=0
    print(pop)

    costs=list(toolbox.map(toolbox.evaluate,pop))
    for ind,cost in zip(pop,costs):
        ind.fitness.values=cost

    costs=[ind.fitness.values[0] for ind in pop]
    print(costs)
    
    while g<number_generations:
        g=g+1
        print(f"--Generation {g}--")
        deb=time.time()
        offspring = toolbox.select(pop, select_size)
        offspring = list(toolbox.map(toolbox.clone, offspring))
        while len(offspring)<pop_size:
            child=rd.choice(offspring)
            offspring.append(toolbox.clone(child))
        print("selection+fill:"+str(time.time()-deb))
        deb=time.time()
        # Apply crossover and mutations
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if rd.random()<crossover_probability:
                if rd.random()<cpga_probability:
                    toolbox.cpga(child1,child2)
                    del child1.fitness.values
                    del child2.fitness.values
                else:
                    toolbox.cmap(child1,child2)
                    del child1.fitness.values
                    del child2.fitness.values
        print("crossover:"+str(time.time()-deb))
        
        deb=time.time()

        #toolbox.map(toolbox.mutate,zip(pop,[mutation_probability for _ in pop]))
        for mutant in offspring:
            
            toolbox.mutate((mutant,mutation_probability))
            del mutant.fitness.values


        print("mutation:"+str(time.time()-deb))
        # Update the fitness of invalid costs
        deb=time.time()
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        costs = list(toolbox.map(toolbox.evaluate, invalid_ind))
        for ind,cost in zip(invalid_ind,costs):
            ind.fitness.values=cost
        print("calculate_new_costs:"+str(time.time()-deb))
        pop[:] = offspring

        # Gather all the costs in one list and print the stats
        costs = [ind.fitness.values[0] for ind in pop]
        
        length = len(pop)
        mean = sum(costs) / length
        sum2 = sum(x*x for x in costs)
        std = abs(sum2 / length - mean**2)**0.5
        
        print("  Min %s" % min(costs))
        print("  Max %s" % max(costs))
        print("  Avg %s" % mean)
        print("  Std %s" % std)

if __name__=="__main__":
    main()
