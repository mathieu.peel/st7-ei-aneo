#generate a graph with each level pointing to each point of the next level so as to have a maximum number of dependencies
#but with a predictable minimum time to test the algorithm
import json

def new_node_from_one(node,nproc,graph_json,nlevel):
    if nlevel==0:
        pass
    else:
        #return nproc node with dependency in node
        for i in range(1,nproc+1):
            node_number=str(node)+':'+str(i)
            Dependencies=[node]

            graph_json["nodes"][node_number]={"Data":"00:00:01.0000000",
            "Dependencies":Dependencies}
            new_node_from_one(node_number,nproc,graph_json,nlevel-1)

nproc=4
nlevel=5
graph_json={"nodes":{"1":
        {
            "Data":"00:00:01.0000000",
            "Dependencies": [
             ]
        }}}
new_node_from_one(1,nproc,graph_json,nlevel)

with open('./Graphs/'+ "P{}L{}".format(nproc,nlevel) + ".json", "w") as f:
    json.dump(graph_json,f)