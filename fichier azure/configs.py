

##################
# Batch account  #
##################

batch = {
    "name": "ab03centralesupelec",
    "key": "GgavlvjW11us7S3QaYknR1pOA2oDMPQnLzBcUJKloyZcXs/xuoAN5oQ4lQ7IgkJSqLVA+R+fF7tj1ZM5MI6MQg==",
    "url": "https://ab03centralesupelec.centralus.batch.azure.com"
}


###################################################
# Blob container which will store results of runs #
###################################################

blob_container = {
    "url": "https://as03centralesupelec.blob.core.windows.net/results",
    "sas_token": "?st=2021-03-22T23%3A28%3A51Z&se=2021-07-20T23%3A29%3A51Z&sp=rwdl&sv=2018-03-28&sr=c&sig=Ppu8YOpjV61q1skL7eTaxotxYFMD6U7K5/fHyFpP9no%3D"
}


######################
# Configuration Pool #
######################

#rule of scability of pool
rule_scaling = (
                '// Get pending tasks for the past 5 minutes.\n'
                '$samples = $ActiveTasks.GetSamplePercent(TimeInterval_Minute * 5);\n'
                '// If we have fewer than 70 percent data points, we use the last sample point, otherwise we use the maximum of last sample point and the history average.\n'
                '$tasks = $samples < 70 ? max(0, $ActiveTasks.GetSample(1)) : '
                'max( $ActiveTasks.GetSample(1), avg($ActiveTasks.GetSample(TimeInterval_Minute * 5)));\n'
                '// If number of pending tasks is not 0, set targetVM to pending tasks, otherwise half of current dedicated.\n'
                '$targetVMs = $tasks > 0 ? $tasks : max(0, $TargetDedicatedNodes / 2);\n'
                '// The pool size is capped. This value should be adjusted according to your use case.\n'
                'cappedPoolSize = 5;\n'
                '$TargetLowPriorityNodes = max(0, min($targetVMs, cappedPoolSize));\n'
                '// Set node deallocation mode - keep nodes active only until tasks finish\n'
                '$NodeDeallocationOption = taskcompletion;'
               )


#####################
# Configuration Job #
#####################

#Repository (Github,Gitlab, etc.) contenant les inputs
repository = 'https://gitlab-student.centralesupelec.fr/mathieu.peel/st7-ei-aneo'

#Commandes de la tâche de préparation(clonage d'un repo Azure Devops et installation des packages python nécessaires pour le process mpi)
cmd_prep_task = (
                  "bash -c 'git clone {0} ; cd st7-ei-aneo ; chmod +x install.sh; ./install.sh'".format(repository)
                 )



######################
# Configuration Task #
######################

nb_processes = 5

#copier le script d'execution dans le dossier partagé du noeud
coordination_command = "bash -c 'ls; cp -vaR $AZ_BATCH_JOB_PREP_DIR/wd/st7-ei-aneo/. $AZ_BATCH_NODE_SHARED_DIR/.; cd $AZ_BATCH_JOB_PREP_DIR; ls; git pull'"

start_command = (
       "bash -c 'mpirun -np {0} -host $AZ_BATCH_HOST_LIST -wdir $AZ_BATCH_NODE_SHARED_DIR python3 $AZ_BATCH_NODE_SHARED_DIR/main2.py ""--infile"" deap_test.txt ""--verbose"";'".format(nb_processes)
      )