#Adapt the integrated module for time to test a probably correct implementation of time

from datetime import datetime, date, timedelta
import re

class time:
    def __init__(self,hours, minutes, seconds, microsec,td_init=False,td=None):
        if td_init==False:
            self.td= timedelta(seconds=seconds,microseconds=microsec,minutes=minutes,hours=hours)
        else:
            self.td=td

    def __repr__(self):
        return str(self.td)
    
    def add(self,time2):
        self.td= self.td+ time2.td
        return self

    def __ge__(self,time2):
        return self.td>=time2.td

    def __gt__(self,time2):
        return self.td>time2.td
    
    def copy(self):
        return self.td #immutable dc pas de pb

    def __truediv__(self,other):
        try:
            return self.td/other.td
        except ZeroDivisionError:
            return 1

    def to_float(self):
        #float du nbre de secondes
        return self.td.total_seconds()

    def copy(self):
        return time(0,0,0,0, td_init=True, td=self.td)


def parser(time_str):
    m= re.search('([0-9]*):([0-9]*):([0-9]*).([0-9]*)', time_str)

    hour = int(m.group(1))
    minutes=int(m.group(2))
    seconds= int(m.group(3))

    if m.group(4)=='':
        microseconds=0
    else:
        microseconds = int(m.group(4)[:-1]) #pas des microsecondes mais plus ?
    return time(hour,minutes,seconds,microseconds)

def add(time1,time2):
    final_td=time1.td+time2.td
    return time(0,0,0,0,td_init=True,td=final_td)