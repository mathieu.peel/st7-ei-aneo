import json
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection

def timetable_plot(data,filepath, save=False):

    cats={}
    verts=[]
    colors=[]

    for processor in data:
        cats["processor {}".format(processor)]=int(processor)

        n_tasks=data[processor]["n_tasks"]
        

        for i in range(1,n_tasks+1):
            ST = data[processor][i]['ST']
            FT = data[processor][i]['FT']
            
            j=int(processor)
            v=[ (ST,j-.4),
            (ST,j+.4),
            (FT,j+.4),
            (FT,j-.4),
            (ST,j-.4),]
            verts.append(v)
            colors.append("C{}".format(i))
            colors.append("orange")

    # plt.style.use('dark_background')
    bars=PolyCollection(verts,facecolors=colors)
    fig,ax=plt.subplots()
    ax.add_collection(bars)
    ax.autoscale()

    labels=[]
    ticks=[]
    for i in cats:
        labels.append(i)
        ticks.append(cats[i])

    ax.set_yticks=(list(range(1,j)))
    if save:
        plt.savefig(filepath)
    else:
        plt.show()
    plt.close()
    pass
