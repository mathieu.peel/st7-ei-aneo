# ST7 EI Aneo

To launch the program now, use :

```
mpiexec /np 12 python main2.py "--infile" deap_test.txt "--verbose"
```

For Linux 

```
mpirun -np 12 python main2.py ""--infile"" deap_test.txt "--verbose"
```

```--infile``` Path to the files describing the problems. One file per problem

``--indir`` Run all problems inside the directory

``--verbose`` To see the training progress

``--loops`` Number of runs

For the first parallelisation method (one algorithm/multiple processor), use
```
main2.py
```

For the second parallelisation method (multiple algorithms/multiple processors), change for
```
main_sequential.py
```

in the command 

# Todo

- meilleur algorithme d'affectations des tâches à partir de l'ordre topologique ?
- scalability issue and parallelisation ideas

- calculer les valeurs moyennes + confidence interval
=> hyperparameter better than another one if better values and no overlapping

parallelisation :
- get a good idea of time repartition between tasks to reduce/parallelize to improve the performance

- reduce the cardinality or complexity of the problem space if possible

# Report 

- Final version of the 2 reports : one less than 2 pages, short, the other one longer => temporary report on Wednesday, and the final one by Friday.
- Need to let the algorithm converge to a local minimum for the report