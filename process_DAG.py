import networkx as nx
import time_module
import json
import itertools
import random as rd

def init_Graph(full_graph) :

    G = nx.DiGraph()
    for node in full_graph.keys():
        G.add_node(str(node))
        for dependence in full_graph[str(node)]['Dependencies']:
            G.add_edges_from([(str(dependence), str(node))])
        G.nodes[str(node)]["time"] = time_module.parser(full_graph[str(node)]['Data'])

    return G

def path_to_full_graph(path):
    with open(path) as json_file:
            data = json.load(json_file)
    full_graph = data["nodes"]
    return full_graph

def RandomTopologicalOrder(G):
    #Create temporary graph datas that will be changed

    N=G.number_of_nodes()
    node_list_used=list(G.nodes())
    indegree = {}

    for node in node_list_used:
        indegree[node]=G.in_degree(node)


    #Start the topological sort
    path=[] #Keep track of the path

    while len(path)<N:
        #Pick a random vertex with indegree 0 (worst way possible)
        rd.shuffle(node_list_used)
        for i in range(len(node_list_used)):
            v=node_list_used[i]
            if indegree[v]==0:
                node_list_used.pop(i)
                break
        
        for u in list(G[v].keys()):
            indegree[u]=indegree[u]-1
        
        path.append(v)

    return path

def cond_noeud(noeud,graph):
    if noeud == None:
        return True
    return graph.in_degree[noeud] <=1 and graph.out_degree[noeud] <=1

def reduction_Graph(networkx_graph):
    nodes_reduc={}
    reduc_map={}
    cpt=0
    for i in networkx_graph.nodes():
        if i not in nodes_reduc:
            if cond_noeud(i,networkx_graph):
                tmp_nodes={i:cpt}

                # print(tmp_nodes)

                noeud_courant_arriere=i
                noeuds_arriere=[]

                change=True
                while cond_noeud(noeud_courant_arriere, networkx_graph) and (change):
                    tmp_nodes[noeud_courant_arriere]=cpt
                    noeuds_arriere.append(noeud_courant_arriere)
                    #print("arrière : {}".format(noeud_courant_arriere))
                    change=False
                    for j in networkx_graph.predecessors(noeud_courant_arriere):
                        change=True
                        noeud_courant_arriere=j
                
                noeuds_avant=[]
                noeud_courant_avant=i
                change=True
                while cond_noeud(noeud_courant_avant, networkx_graph) and (change) and noeud_courant_avant:
                    tmp_nodes[noeud_courant_avant]=cpt
                    noeuds_avant.append(noeud_courant_avant)
                    #print("avant : {} ".format(noeud_courant_avant))
                    change=False
                    for j in networkx_graph.neighbors(noeud_courant_avant):
                        change=True
                        noeud_courant_avant=j
                #print(tmp_nodes)
                if len(tmp_nodes)>1:
                    for j in tmp_nodes:
                        nodes_reduc[j]=cpt
                    noeuds_arriere=noeuds_arriere[1:]
                    reduc_map[cpt]= noeuds_arriere[::-1] + noeuds_avant
                    # if cpt==5:
                        #print(tmp_nodes, noeuds_arriere,noeuds_avant,i, reduc_map)
                    cpt+=1
    # print(cpt, len(nodes_reduc))
    return nodes_reduc,reduc_map

def graph_rewrite_from_reduction(networkx_graph):
    n,r=reduction_Graph(networkx_graph)


    for cpt in r:
        nodes_to_change=r[cpt]

        #Calculate total equivalent time
        TEQ = time_module.time(0, 0, 0, 0)
        new_name=str(nodes_to_change)
        
        for node in nodes_to_change:
            TEQ.add(networkx_graph.nodes[node]["time"])
        
        #Add a new node
        networkx_graph.add_node(new_name)
        networkx_graph.nodes[new_name]["time"]=TEQ

    for cpt in r:
        nodes_to_change=r[cpt]
        new_name=str(nodes_to_change)
        #we have to keep the nodes to establish the dependencies
        for j in networkx_graph.neighbors(nodes_to_change[-1]):
            if j not in n:
                networkx_graph.add_edges_from([(new_name,j)])
            else:
                new_node=str(r[n[j]])
                networkx_graph.add_edges_from([(new_name,new_node)])

        for j in networkx_graph.predecessors(nodes_to_change[0]):
            if j not in n:
                networkx_graph.add_edges_from([(j,new_name)])
            else:
                new_node=str(r[n[j]])
                networkx_graph.add_edges_from([(new_node,new_name)])

    #All the new dependencies are added, remove old nodes
    for cpt in r:
        nodes_to_change=r[cpt]
        for node in nodes_to_change:
            networkx_graph.remove_node(node)
    return networkx_graph
    