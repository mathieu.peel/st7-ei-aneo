import re
import numpy as np

class Parser:
    def __init__(self, infile):
        self.data = {}
        with open(infile) as fd:
            for line in fd:
                line = str.strip(re.sub(r'#.*$', '', line))
                if line == '':
                    continue
                toks = re.split(r'<|>', line)
                if len(toks) != 3:
                    raise 'Format error in ' + infile
                _label = str.strip(toks[0])
                _type = str.strip(toks[1])
                _value = str.strip(toks[2])
                if _type == 'int' or _type == 'long':
                    _value = int(_value)
                elif _type == 'float':
                    _value = float(_value)
                elif _type == 'vector':
                    vals = _value.split(' ')
                    _value = np.array([float(x) for x in vals])
                self.data.update({_label: {'type': _type, 'value': _value}})

    def __str__(self):
        return str(self.data)

    def get(self, label, raiseError=True, defaultValue=None):
        try:
            return self.data[label]['value']
        except KeyError as e:
            if not raiseError:
                return defaultValue
            else:
                print('Key ' + label + ' not found.')
                raise e


    def getVector(self, label, n, raiseError=True):
        try:
            if self.data[label]['type'] != 'vector':
                raise 'Element ' + label + ' is not of type vector'
            v = self.data[label]['value']
            if np.size(v) == 1:
                return np.full(n, v)
            else:
                return v
        except KeyError as e:
            if not raiseError:
                return None
            else:
                print('Key ' + label + ' not found.')
                raise e
