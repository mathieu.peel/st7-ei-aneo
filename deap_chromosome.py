import random as rd
import json
import numpy as np
import process_DAG
import time_module
import time
import mpi4py.futures
from mpi4py import MPI
import sys
import matplotlib.pyplot as plt

from deap import base
from deap import creator
from deap import tools
from Analyser import Parser
from pathlib import Path

from process_DAG import init_Graph, RandomTopologicalOrder

import timetable_visualisation

MPI_COMM=MPI.COMM_WORLD
MPI_SIZE=MPI_COMM.Get_size()
MPI_RANK=MPI_COMM.Get_rank()

class deap_genetic:
    def fit(self, x, y):
        raise NotImplementedError

    def mutation(self,x, y):
        raise NotImplementedError

    def crossover(self, x, y):
        raise NotImplementedError

    def evaluate(self, individual):
        raise NotImplementedError
    
    def evaluate_and_save(self, individual):
        raise NotImplementedError

class Genetic(deap_genetic):
    def __init__(self, P: Parser, verbose=False):
        self.NbProc = P.get('Number of processors')
        self.Graphpath = P.get('Graph path')
        self.PopSize = P.get('Population size')
        self.SelectSize = P.get('Selection size')
        self.nGenerations = P.get('Number of generations')
        self.weights = P.get('Weights')
        #self.FitnessName = P.get('Fitness name')
        #self.IndName = P.get('Individual name')
        self.cpga_prob = P.get('cpga probability')
        self.cmap_prob = 1-self.cpga_prob
        self.cross_prob = P.get('crossover probability')
        self.mut_prob = P.get('mutation probability')
        self.verbose = verbose
        self.reduce=P.get('reduction')
        self.plot=P.get('plot')
        self.SaveStats=P.get('save stats')
        self.mutate_order=P.get('mutate order')
        
        self.mutate_order_gene=P.get("mutate order each n generations")
        
        with open(self.Graphpath) as json_file:
            data = json.load(json_file)
        full_graph = data["nodes"]
        g=process_DAG.init_Graph(full_graph)

        if self.reduce==1:
            g= process_DAG.graph_rewrite_from_reduction(g)
            if MPI_RANK==0:
                print("reducing the graph")
        
        self.Graph =  g 



        self.NbNodes = self.Graph.number_of_nodes()

    def init_inidividual(self):
        proc_list=[rd.randint(1, self.NbProc) for _ in range(self.Graph.number_of_nodes())]
        cyclic_reduction(proc_list,self.NbProc)
        return np.array([RandomTopologicalOrder(self.Graph), proc_list])

    def crossover_map(self, ind1, ind2):
        n = len(ind1[0])
        crossover_point=rd.randint(0,n-1)
        save=np.copy(ind1[1,crossover_point:])
        ind1[1,crossover_point:]=np.copy(ind2[1,crossover_point:])
        ind2[1,crossover_point:]=save
        cyclic_reduction(ind1[1],self.NbProc)
        cyclic_reduction(ind2[1],self.NbProc)

    def crossover_cpga(self, ind1, ind2):  # Conserve dans chaque individu son début
        n = len(ind1[0])
        crossover_point = rd.randint(0, n - 1)
        ind1_original, ind2_original = np.copy(ind1), np.copy(ind2)
        ind1_c, ind2_c = np.copy(ind1[:, crossover_point:]), np.copy(ind2[:, crossover_point:])

        ind1_c_0 = ind1_c[0, :]
        ind1_c_0.flatten()
        ind2_c_0 = ind2_c[0, :]
        ind2_c_0.flatten()

        ind1_0 = np.copy(ind1[0, :])
        ind1_0.flatten()
        ind2_0 = np.copy(ind2[0, :])
        ind2_0.flatten()

        l_index_2, l_index_1 = [], []
        for i, j in zip(ind1_c_0, ind2_c_0):
            l_index_2.append(np.where(ind2_0 == i)[0][0])
            l_index_1.append(np.where(ind1_0 == j)[0][0])
        l_index_1.sort()
        l_index_2.sort()

        ind1[:, crossover_point:] = ind2_original[:, l_index_2]
        ind2[:, crossover_point:] = ind1_original[:, l_index_1]

    def mutation_cpga(self,ind1,size_window,prob,toolbox): #New mutation used for nudging the topological order
        if rd.random() < prob:
            ind2=toolbox.Individual()
            ind2[1,:]=ind1[1,:]
            n = len(ind1[0])
            crossover_point = rd.randint(0, n - 1-size_window)
            crossover_point_2 = crossover_point+size_window

            ind2_original = np.copy(ind2)
            ind1_c, ind2_c = np.copy(ind1[:, crossover_point:crossover_point_2]), np.copy(ind2[:, crossover_point:crossover_point_2])

            ind1_c_0 = ind1_c[0, :]
            ind1_c_0.flatten()
            ind2_c_0 = ind2_c[0, :]
            ind2_c_0.flatten()

            ind2_0 = np.copy(ind2[0, :])
            ind2_0.flatten()

            l_index_2= []
            for i in ind1_c_0:
                l_index_2.append(np.where(ind2_0 == i)[0][0])
            l_index_2.sort()

            ind1[:, crossover_point:crossover_point_2] = ind2_original[:, l_index_2]
            sys.stdout.flush()
            del ind1.fitness.values
        return toolbox.clone(ind1)


    def mutation(self, arg):
        ind, prob = arg
        n = len(ind[0])
        if rd.random() < prob:
            for i in range(n):
                if rd.random() < 0.05:
                    random_processor = rd.randint(1, self.NbProc)
                    ind[1][i] = random_processor
                    del ind.fitness.values
            cyclic_reduction(ind[1],self.NbProc)


    def evaluate(self, individual):
        # implement the schedule length function as seen in page 4 of the paper
        #m Nb of processors

        

        RT = [time_module.time(0, 0, 0, 0) for _ in range(self.NbProc)]
        processors = list(individual[1]).copy()
        LT = list(individual[0]).copy()

        ST = {}
        FT = {}
        empty_slots={i:{"nb_empty":0} for i in range(self.NbProc)}

        for i in range(self.NbNodes):
            task = LT.pop(0)
            Pj = int(processors[i])
            predecessors = list(self.Graph.predecessors(task))
            DAT = time_module.time(0, 0, 0, 0)

            for dependency in predecessors:
                if FT[str(dependency)] >= DAT:
                    DAT = FT[str(dependency)].copy()
            #print("DAT", DAT)
            #print("RT", Pj -1)
            if DAT >= RT[Pj - 1]:
                ST[task] = DAT.copy()
                #We just created a empty slot between RT[Pj-1] and DAT
                nb_empty=empty_slots[Pj-1]['nb_empty']
                empty_slots[Pj-1][nb_empty]={"length":DAT.to_float()-RT[Pj-1].to_float(),"ST":RT[Pj-1].copy(),"FT":DAT.copy()}
                empty_slots[Pj-1]['nb_empty']=nb_empty+1
                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                RT[Pj - 1] = FT[task].copy()
            else:
                #We might want to put the starting point of this task in an empty slot if at all possible
                empty_filled=False
                time_task=self.Graph.nodes[task]["time"].to_float()
                for i in list(empty_slots[Pj-1])[1:]:
                    if empty_filled==False:
                        if empty_slots[Pj-1][i]["ST"] > DAT:
                            if empty_slots[Pj-1][i]["length"]>time_task:
                                ST[task]=empty_slots[Pj-1][i]["ST"]
                                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                                
                                #change the slot :
                                if empty_slots[Pj-1][i]["FT"]> FT[task]:
                                    empty_slots[Pj-1][i]["ST"]=FT[task].copy()
                                    empty_slots[Pj-1][i]["length"]=empty_slots[Pj-1][i]["FT"].to_float()-empty_slots[Pj-1][i]["ST"].to_float()
                                else:
                                    #delete the slot
                                    empty_slots[Pj-1].pop(i)

                                empty_filled=True
                if empty_filled==False:
                    ST[task] = RT[Pj - 1].copy()
                    FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                    RT[Pj - 1] = FT[task].copy()
        Final_time = time_module.time(0, 0, 0, 0)
        for i in FT.keys():
            if FT[i] >= Final_time:
                Final_time = FT[i]
        #print("FT",Final_time)
        #print("REPE", Final_time.to_float())
        return (Final_time.to_float(),)
    
    def evaluate_and_save(self,individual):
        # implement the schedule length function as seen in page 4 of the paper
        #m Nb of processors
        #We add some parameters to also get the worst time possible (time on one processor) by adding all the times
        #And another to get the best time possible, that is to say with no affectation on processors


        timetable={i+1:{"n_tasks":0} for i in range(self.NbProc)}

        RT = [time_module.time(0, 0, 0, 0) for _ in range(self.NbProc)]
        processors = list(individual[1]).copy()
        LT = list(individual[0]).copy()
        
        ST = {}
        ST_inf={}

        FT = {}
        FT_inf={}

        empty_slots={i:{"nb_empty":0} for i in range(self.NbProc)}


        worst_time=time_module.time(0,0,0,0)

        for i in range(self.NbNodes):
            task = LT.pop(0)
            Pj = int(processors[i])
            predecessors = list(self.Graph.predecessors(task))
            DAT = time_module.time(0, 0, 0, 0)
            DAT_inf= time_module.time(0,0,0,0)
            for dependency in predecessors:
                if FT[str(dependency)] >= DAT:
                    DAT = FT[str(dependency)].copy()

                if FT_inf[str(dependency)] >= DAT:
                    DAT_inf = FT[str(dependency)].copy()
            #print("DAT", DAT)
            #print("RT", Pj -1)
            if DAT >= RT[Pj - 1]:
                ST[task] = DAT.copy()
                
                #We just created a empty slot between RT[Pj-1] and DAT
                nb_empty=empty_slots[Pj-1]['nb_empty']
                empty_slots[Pj-1][nb_empty]={"length":DAT.to_float()-RT[Pj-1].to_float(),"ST":RT[Pj-1].copy(),"FT":DAT.copy()}
                empty_slots[Pj-1]['nb_empty']=nb_empty+1
                
                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                RT[Pj - 1] = FT[task].copy()

            else:
                #We might want to put the starting point of this task in an empty slot if at all possible
                empty_filled=False
                time_task=self.Graph.nodes[task]["time"].to_float()
                for i in list(empty_slots[Pj-1])[1:]:
                    if empty_filled==False:
                        if empty_slots[Pj-1][i]["ST"] > DAT:
                            if empty_slots[Pj-1][i]["length"]>time_task:
                                ST[task]=empty_slots[Pj-1][i]["ST"]
                                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                                
                                #change the slot :
                                if empty_slots[Pj-1][i]["FT"]> FT[task]:
                                    empty_slots[Pj-1][i]["ST"]=FT[task].copy()
                                    empty_slots[Pj-1][i]["length"]=empty_slots[Pj-1][i]["FT"].to_float()-empty_slots[Pj-1][i]["ST"].to_float()
                                else:
                                    #delete the slot
                                    empty_slots[Pj-1].pop(i)

                                empty_filled=True
                if empty_filled==False:
                    ST[task] = RT[Pj - 1].copy()
                    FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                    RT[Pj - 1] = FT[task].copy()

            ST_inf[task]=DAT_inf.copy()
            FT_inf[task]=time_module.add(ST_inf[task], self.Graph.nodes[task]["time"])

            worst_time.add(self.Graph.nodes[task]["time"])            

            #we start the task "task" on processor Pj starting at ST and finishing at FT
            nb_tasks=timetable[Pj]["n_tasks"]
            timetable[Pj][nb_tasks+1]={"node":task,"ST":ST[task].to_float(),"FT":FT[task].to_float()}
            timetable[Pj]["n_tasks"]= nb_tasks+1

        Final_time = time_module.time(0, 0, 0, 0)
        Final_time_inf=time_module.time(0,0,0,0)
        for i in FT.keys():
            if FT[i] >= Final_time:
                Final_time = FT[i]
            if FT_inf[i] >= Final_time_inf:
                Final_time_inf = FT_inf[i]
        # print("FT",Final_time)
        # print("FTINF",Final_time_inf)
        #print("REPE", Final_time.to_float())
        return (Final_time,timetable,worst_time,Final_time_inf)        

    def preselect_all(self,pop,toolbox, select_size): #Utilisé sur toute la population dans rank 0
        if MPI_RANK!=0:
            return None
        
        offspring = toolbox.select(pop,select_size)
        offspring = [toolbox.clone(x) for x in offspring]

        return offspring

    def init_deap(self, toolbox):
        #creator.create("Fitness", base.Fitness, weights=(self.weights,))
        #creator.create("Individual", np.ndarray, fitness=creator.Fitness)
        #self.toolbox = base.Toolbox()
        toolbox.register("Init_ind", self.init_inidividual)
        toolbox.register("Individual", tools.initIterate, creator.Individual,toolbox.Init_ind)
        toolbox.register("Population", tools.initRepeat, list, toolbox.Individual)
        toolbox.register("cmap", self.crossover_map)
        toolbox.register("cpga", self.crossover_cpga)
        toolbox.register("evaluate", self.evaluate)
        toolbox.register("evaluate_and_save", self.evaluate_and_save)
        toolbox.register("select", tools.selTournament, tournsize=8) #8 because approximately self.PopSize/10

        toolbox.register("mutate_order",self.mutation_cpga)
        toolbox.register("mutate", self.mutation)
        #self.toolbox.register("map", pool.map)
        #if self.verbose :
            #print('Using ' + str(mp.cpu_count()) + ' processors.')

    def eval_pop(self,pop,toolbox): #On utilise scatter et gather
        data_in = None
        #keys = None
        sys.stdout.flush()
        if MPI_RANK == 0:
            data_in = []
            #keys = []
            length = int(len(pop) / MPI_SIZE)+1
            sys.stdout.flush()
            for i in range(MPI_SIZE):
                start = i * length
                stop = min(start + length,len(pop))
                data_in.append(pop[start:stop])
                #keys.append([ii for ii in range(start, stop)])
            '''
            j = 0
            for i in range(stop, len(pop)):
                data_in[j] += [pop[i]]
                keys[j] += [i]
                j += 1
            '''
        data_in = MPI_COMM.scatter(data_in, root=0)
        #keys = MPI_COMM.scatter(keys, root=0)
        
        if data_in!=None:
            costs_proc=[toolbox.evaluate(ind) for ind in data_in]
        else:
            costs_proc=[]
        data_out=MPI_COMM.gather(costs_proc,root=0) # L'odre est bien concervé

        if MPI_RANK==0:
            costs=[]
            for a in data_out:
                costs+=a
        
            for ind, cost in zip(pop, costs):
                ind.fitness.values = cost
            if self.verbose:
                print(f"Evaluated {str(len(pop))} individuals")
                sys.stdout.flush()
        else:
            pop=None

        return pop

    def mutate_population_order(self,pop,toolbox):
        data_in = None
        #keys = None
        sys.stdout.flush()
        if MPI_RANK == 0:
            data_in = []
            #keys = []
            length = int(len(pop) / MPI_SIZE)+1
            sys.stdout.flush()
            for i in range(MPI_SIZE):
                start = i * length
                stop = min(start + length,len(pop))
                data_in.append([toolbox.clone(x) for x in pop[start:stop]])
                #data_in.append(pop[start:stop])
                #keys.append([ii for ii in range(start, stop)])
            '''
            j = 0
            for i in range(stop, len(pop)):
                data_in[j] += [pop[i]]
                keys[j] += [i]
                j += 1
            '''
        
        data_in = MPI_COMM.scatter(data_in, root=0)
        #keys = MPI_COMM.scatter(keys, root=0)
        
        for i in range(len(data_in)):
            #toolbox.mutate((mutant, self.mut_prob))
            data_in[i]=self.mutation_cpga(data_in[i],len(data_in[i][0])//10,self.mut_prob,toolbox)
        
        
        data_out=MPI_COMM.gather(data_in,root=0)
        if MPI_RANK==0:
            pop2=[]
            for a in data_out:
                pop2+=a
            
            for i in range(len(pop)):
                pop[i]=pop2[i]
        
        
            #return pop2
        
        '''
        else:
            return None
        '''

    def fit(self, toolbox):
        """
        # Create initial population
        """
        #print("hello from process :",MPI_RANK)
        #pool=mpi4py.futures.MPIPoolExecutor(max_workers=size)
        #toolbox.register("map", pool.map)
        if self.verbose :
            #print('Using ' + str(mp.cpu_count()) + ' processors.')
            if MPI_RANK==0:
                print("Number of processors:",MPI_SIZE)
                absolute_start_time=time.time()
                sys.stdout.flush()
        pop_proc=toolbox.Population(int(self.PopSize/MPI_SIZE))
        costs_proc = [toolbox.evaluate(ind) for ind in pop_proc]
        for ind, cost in zip(pop_proc, costs_proc):
            ind.fitness.values = cost
        costs_proc = [ind.fitness.values[0] for ind in pop_proc]
        
        pop_scattered=MPI_COMM.gather(pop_proc,root=0)
        if MPI_RANK==0:
            pop=[]
            current_min=None
            min_list=[]
            max_list=[]
            avg_list=[]
            std_list=[]
            #initialisation à 0 pr simplifier le plot avec initialisation
            sel_fill_time=[0]
            cross_over_time=[0]
            mutation_time=[0]
            calculate_costs_time=[0]
            for a in pop_scattered:
                pop+=a
            print(len(pop))
        else:
            pop=None
        #pop = list(toolbox.map(toolbox.Individual, [toolbox.Init_ind for _ in range(self.PopSize/)]))
        g = 0
        
        if self.verbose and MPI_RANK==0:
            print("Initial costs for rank 0:", costs_proc)
            init_time=time.time()-absolute_start_time
            print("time for initialisation : {}".format(init_time))
            
            sys.stdout.flush()
        
        while g < self.nGenerations:
            g += 1
            if self.verbose and MPI_RANK==0:
                sys.stdout.flush()
                print(f"--Generation {g}--")
            if MPI_RANK==0:
                start_time = time.time()
            offspring = self.preselect_all(pop, toolbox, self.SelectSize)
            if MPI_RANK==0:
                while len(offspring)<self.PopSize:
                    offspring.append(toolbox.clone(offspring[rd.randint(0,self.SelectSize-1)]))
                rd.shuffle(offspring)
            
            if self.verbose and MPI_RANK==0:
                t1=time.time() - start_time
                sel_fill_time.append(t1)
                print("selection+fill:" + str(t1))
                sys.stdout.flush()
                start_time = time.time()
            
            # Apply crossover and mutations

            #### Pour le moment on ça sur le noeud principal, à voir si c'est mieux de scatter puis gather ####
            if MPI_RANK==0:
                for child1, child2 in zip(offspring[::2], offspring[1::2]):
                    if rd.random() < self.cross_prob:
                        if rd.random() < self.cpga_prob:
                            toolbox.cpga(child1, child2)
                            del child1.fitness.values
                            del child2.fitness.values
                        else:
                            toolbox.cmap(child1, child2)
                            del child1.fitness.values
                            del child2.fitness.values

            if self.verbose and MPI_RANK==0:
                t1=time.time() - start_time
                cross_over_time.append(t1)
                print("crossover :" + str(t1))
                sys.stdout.flush()
                start_time = time.time()

            #need to update the parameter if the second were to be used
            if g%self.mutate_order_gene <= 4: #or len(list(set(min_list[-100:]))):#No update
                if MPI_RANK==0:
                    if rd.random()< self.mutate_order:
                        order=1
                    else:
                        order=0
                else:
                    order=0
                order=MPI_COMM.bcast(order, root=0)
                if order==1:
                    #offspring = self.mutate_population_order(offspring,toolbox)
                    self.mutate_population_order(offspring,toolbox)
                else:
                    if MPI_RANK==0:
                        for mutant in offspring:
                            toolbox.mutate((mutant, self.mut_prob))
                
                '''if MPI_RANK==0:  
                    else:
                        toolbox.mutate_order(mutant,len(mutant[0])//20,self.mut_prob,toolbox)'''
            else:
                if MPI_RANK==0:
                    for mutant in offspring:
                        toolbox.mutate((mutant, self.mut_prob))
            
            if self.verbose and MPI_RANK==0:
                t1=time.time()-start_time
                mutation_time.append(t1)
                print("mutation:" + str(t1))
        

            # Update the fitness of invalid costs

            if MPI_RANK==0:
                start_time = time.time()
                invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            else:
                invalid_ind=None

            if MPI_RANK==0:
                start_time = time.time()
            invalid_ind=self.eval_pop(invalid_ind,toolbox)


            if self.verbose and MPI_RANK==0:
                t1=time.time()-start_time
                calculate_costs_time.append(t1)
                print("calculate_new_costs:" + str(t1))
            
            if MPI_RANK==0:
                pop[:] = offspring

            else:
                pop=None
            # Gather all the costs in one list and print the stats
            if MPI_RANK==0:
                costs=[]
                for ind in pop:
                    current_fit=ind.fitness.values[0]
                    if current_min == None or current_fit <= current_min:
                        best_ind=ind
                        current_min=current_fit
                    costs.append(current_fit)
                

                length = len(pop)
                mean = sum(costs) / length
                sum2 = sum(x * x for x in costs)
                std = abs(sum2 / length - mean ** 2) ** 0.5

                min_list.append(min(costs))
                max_list.append(max(costs))
                avg_list.append(mean)
                std_list.append(std)

                if self.verbose:
                    print("  Min %s" % min(costs))
                    print("  Max %s" % max(costs))
                    print("  Avg %s" % mean)
                    print("  Std %s" % std)

                if g == self.nGenerations:
                    #time log :
                    total_time=time.time()-absolute_start_time
                    print("total time : {} s".format(total_time))

                    # time data :     
                    #recuperate the best individual
                    final_time,timetble,worst_time,time_inf=toolbox.evaluate_and_save(best_ind)
                    print("Best time found : \t\t\t\t {} \t so \t {} s".format(final_time, final_time.to_float()))
                    print("Time on one processor : \t\t\t {} \t so \t {} s".format(worst_time, worst_time.to_float()))
                    print("Time on one processor/number of processors : \t {} \t so \t {} s".format( time_module.time(0,0,0,0,td_init=True,td=worst_time.td/self.NbProc),worst_time.to_float()/self.NbProc))
                    print("Time on infinite core processor : \t\t {} \t\t so \t {} s".format(time_inf, time_inf.to_float()))
                    
                    if self.plot>=1:
                        #arguments for files
                        Grphpath = self.Graphpath.replace('/','')
                        file_precision = "Nb_Processor={}_Graph={}_Pop_size={}_Gene={}".format(self.NbProc,Grphpath, self.PopSize, self.nGenerations)
                        file_precision+="Nb_proc_MPI={}_Sel_size={}_cpga_prob={}_cross_prob={}_mut_prob={}".format(MPI_SIZE, self.SelectSize,self.cpga_prob,self.cross_prob,self.mut_prob)
                        file_precision+="reduce={}_mutate_order={}".format(self.reduce,self.mutate_order)
                        file_precision+="mutate order each {} generations".format(self.mutate_order_gene)
                        #Plot the result (min, max, avg, std)
                        fig= plt.figure()
                        heights=[5,1]
                        grid=plt.GridSpec(2,3,wspace=0.25,hspace=0.3,height_ratios=heights)
                        ax1=fig.add_subplot(grid[0,0])
                        ax2=fig.add_subplot(grid[0,1])
                        ax3=fig.add_subplot(grid[0,2])
                        ax4=fig.add_subplot(grid[1,:])

                        fig.subplots_adjust(top=0.8,bottom=0.2,wspace=0.25)
                        fig.suptitle("MPI parameter :  {} processors used to compute\n ".format(MPI_SIZE)
                        + "Parameters of the genetic algorithm : Nb Processors : {} ".format(self.NbProc) +
                        "Graph used : {} ".format(Grphpath) + 
                        "Pop size : {} ".format(self.PopSize) +
                        "number of generations : {} ".format( self.nGenerations) +
                        "selection size : {}\n".format(self.SelectSize)+
                        "cpga probability : {} ".format(self.cpga_prob) +
                        "crossover probability : {} ".format(self.cross_prob)+
                        "mutation probability : {} ".format(self.mut_prob)+
                        "reduce: {} mutate_order : {}".format(self.reduce,self.mutate_order) +
                        "mutate order each {} generations".format(self.mutate_order_gene)

                        ,y=0.98)

                        #We might want to plot the best time, worst time and theoretical best time possible but this might make our graph hard to read
                        #The speedup should approach, in the ideal case, the time on 1 processor divided by m
                        # ax1.plot([i+1 for i in range(self.nGenerations)], [worst_time.to_float()/self.NbProc for i in range(self.nGenerations)], label="th. best time")

                        ax1.plot([i+1 for i in range(self.nGenerations)], min_list, label="min")
                        ax1.plot([i+1 for i in range(self.nGenerations)], avg_list, label="avg")
                        ax1.plot([i+1 for i in range(self.nGenerations)], max_list, label="max")
                        ax1.fill_between([i+1 for i in range(self.nGenerations)], min_list,max_list, alpha=0.2, color="black")
                        ax1.legend()
                        ax1.set_xticks([i+1 for i in range(self.nGenerations)],[i+1 for i in range(1,self.nGenerations,10)])
                        ax1.set_title("Min, average, and max time for each generation",pad=20)
                        ax1.set(xlabel="generations",ylabel="time")
                        ax1.ticklabel_format(style="sci",axis="y", scilimits=(0,0))
                        ax1.grid(True)
                        
                        ax2.plot([i+1 for i in range(self.nGenerations)], std_list, label="std")
                        ax2.fill_between([i+1 for i in range(self.nGenerations)], [0 for i in range(self.nGenerations)],std_list, alpha=0.2, color="black")
                        ax2.legend()
                        ax2.set_xticks([i+1 for i in range(self.nGenerations)],[i+1 for i in range(1,self.nGenerations,10)])
                        ax2.set_title("standard deviation for each generation", pad=20)
                        ax2.set(xlabel="generations",ylabel="time")
                        ax2.ticklabel_format(style="sci",axis="y", scilimits=(0,0))
                        ax2.grid(True)


                        #time representation : dans l'ordre : init time, selection + fill, crossover, mutations, calculate new costs
                        labels=[i+1 for i in range(-1,self.nGenerations)]

                        it=[init_time] + [0 for i in range(self.nGenerations)]

                        ax3.grid(zorder=0)
                        ax3.bar(labels,it,label="init",zorder=3)
                        ax3.bar(labels,sel_fill_time,bottom=it,label="selection + fill",zorder=3)
                        ax3.bar(labels,cross_over_time,bottom=sel_fill_time,label="crossover",zorder=3)
                        ax3.bar(labels,mutation_time,bottom=cross_over_time,label="mutation",zorder=3)
                        ax3.bar(labels,calculate_costs_time,bottom=mutation_time,label="calculate new costs",zorder=3)
                        ax3.legend()
                        bottom,top=ax3.get_ylim()
                        ax3.set_ylim(bottom, top*1.3)
                        ax3.set_title("Time per generation\n total time : {}".format(total_time), pad=20)
                        ax3.set(xlabel="generations",ylabel="time")

                        #ax4 plot : to have a visual representation of the total percentage
                        y=[0]
                        ax4.barh(y,it[0]/total_time,color="C0")
                        partial = it[0]/total_time
                        for i in range(len(sel_fill_time)):
                            ax4.barh(y, sel_fill_time[i]/total_time, left=partial, color=(0.5 + i/(2*(self.nGenerations+1)),1/2*(0.5 + i/(2*(self.nGenerations+1))),0))
                            partial+=sel_fill_time[i]/total_time

                        for i in range(len(cross_over_time)):
                            ax4.barh(y, cross_over_time[i]/total_time, left=partial, color=(0,0.5 + i/(2*(self.nGenerations+1)),0))
                            partial+=cross_over_time[i]/total_time

                        for i in range(len(mutation_time)):
                            ax4.barh(y, mutation_time[i]/total_time, left=partial, color=(0.5 + i/(2*(self.nGenerations+1)),0,0))
                            partial+=mutation_time[i]/total_time

                        for i in range(len(calculate_costs_time)):
                            ax4.barh(y, calculate_costs_time[i]/total_time, left=partial, color=(0.9*(0.5 + i/(2*(self.nGenerations+1))),0,0.9*(0.5 + i/(2*(self.nGenerations+1)))))
                            partial+=calculate_costs_time[i]/total_time
                        ax4.set_yticks([])

                        ax4.set_title('repartition of time between tasks', y=-0.8)

                        plt.text(0,-1.4,"Best time found :",ma="left")
                        plt.text(0.30,-1.4,"{}".format(final_time),ma="left")
                        plt.text(0.50,-1.4,"so {} s".format(final_time.to_float()),ma="right")

                        plt.text(0, -1.7, "Theoretical best time on {} processors".format(self.NbProc), ma="left")
                        plt.text(0.30,-1.7,"{}".format(time_module.time(0,0,0,0,td_init=True, td=worst_time.td/self.NbProc)),ma="left")
                        plt.text(0.50,-1.7,"so {} s".format(worst_time.to_float()/self.NbProc),ma="right")

                        plt.text(0, -2, "Theoretical best time on $\infty$ processors", ma="left")
                        plt.text(0.30,-2,"{}".format(time_inf),ma="left")
                        plt.text(0.50,-2,"so {} s".format(time_inf.to_float()),ma="right")

                        plt.text(0, -2.3, "Time on one processor", ma="left")
                        plt.text(0.30,-2.3,"{}".format(worst_time),ma="left")
                        plt.text(0.50,-2.3,"so {} s".format(worst_time.to_float()),ma="right")

                        fig.set_size_inches(15,10)
                        plt.savefig("./output/PLOT_"+file_precision + ".pdf")
                        plt.close()


                        #to save the parameters found
                        best_individual={"Nb_processors":self.NbProc,"Graph":self.Graphpath,"final_time":final_time.to_float(), "th_best_time":worst_time.to_float()/self.NbProc,
                                        "time_inf_proc":time_inf.to_float(),"time_1_proc":worst_time.to_float(),
                                        "pop_size":self.PopSize, "sel_size":self.SelectSize, "MPI_proc":MPI_SIZE,
                                        "n_gene":self.nGenerations, "cpga_prob":self.cpga_prob, "crossover_prob":self.cross_prob,
                                        "mutation_prob":self.mut_prob, "reduce":self.reduce, "mutate_oder":self.mutate_order, "mutate_order_gene":self.mutate_order_gene,
                                        "tasks":list(best_ind[0]),"processors":list(best_ind[1]),"timetable":timetble}
                        with open("./output/" +file_precision+".json", "w") as f:
                            json.dump(best_individual, f,indent=2)

                        #graphical representation (very costly, may need to refrain from using it on larger graphs)
                        if self.plot==2:
                            timetable_visualisation.timetable_plot(timetble,"./output/TIME_"+file_precision+".svg",save=True)

                    if self.SaveStats==1:
                        stats={"Nb_processors":self.NbProc,"Graph":self.Graphpath,"final_time":final_time.to_float(), "th_best_time":worst_time.to_float()/self.NbProc,
                                        "time_inf_proc":time_inf.to_float(),"time_1_proc":worst_time.to_float(),
                                        "pop_size":self.PopSize, "sel_size":self.SelectSize, "MPI_proc":MPI_SIZE,
                                        "n_gene":self.nGenerations, "cpga_prob":self.cpga_prob, "crossover_prob":self.cross_prob,
                                        "mutation_prob":self.mut_prob, "reduce":self.reduce, "mutate_oder":self.mutate_order, "mutate_order_gene":self.mutate_order_gene, "min_list":min_list,"max_list":max_list,"avg_list":avg_list,"std_list":std_list,
                                        'init_time':init_time,"sel_fill_time":sel_fill_time,"crossover_time":cross_over_time,"mutation_time":mutation_time,"calculate_cost_time":calculate_costs_time}
                        with open("./output/STATS" +file_precision+".json", "w") as f:
                            json.dump(stats, f,indent=2)

def cyclic_reduction(ind,nproc):
    #get a unique representation by forcing the first processor to one
    #and each new unique change to the next processor
    proc_count=1
    indice_proc=0
    renumber={}
    while proc_count <= nproc and indice_proc<=len(ind)-1:
        if ind[indice_proc] not in renumber:
            renumber[ind[indice_proc]]=str(proc_count)
            proc_count +=1
        indice_proc+=1
    
    #renumber
    for i in range(len(ind)):
        ind[i] = renumber[ind[i]]