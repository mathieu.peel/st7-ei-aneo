import random as rd
import json
import numpy as np
import process_DAG
import time_module
import time
import mpi4py.futures
from mpi4py import MPI
import sys
import matplotlib.pyplot as plt

from deap import base
from deap import creator
from deap import tools
from Analyser import Parser

from pathlib import Path

from process_DAG import init_Graph, RandomTopologicalOrder

import timetable_visualisation

# MPI_COMM=MPI.COMM_WORLD
# MPI_SIZE=MPI_COMM.Get_size()
# MPI_RANK=MPI_COMM.Get_rank()

class deap_genetic:
    def fit(self, x, y):
        raise NotImplementedError

    def mutation(self,x, y):
        raise NotImplementedError

    def crossover(self, x, y):
        raise NotImplementedError

    def evaluate(self, individual):
        raise NotImplementedError
    
    def evaluate_and_save(self, individual):
        raise NotImplementedError

class Genetic(deap_genetic):
    def __init__(self, P: Parser, graph, FT_in, FT_out, rank,verbose=True): #local_graph, rank, FT_in, FT_out
        self.NbProc = P.get('Number of processors')
        self.Graphpath = P.get('Graph path')
        self.PopSize = P.get('Population size')
        self.SelectSize = P.get('Selection size')
        self.nGenerations = P.get('Number of generations')
        self.weights = P.get('Weights')
        self.cpga_prob = P.get('cpga probability')
        self.cmap_prob = 1-self.cpga_prob
        self.cross_prob = P.get('crossover probability')
        self.mut_prob = P.get('mutation probability')
        self.verbose = verbose
        self.reduce=P.get('reduction')
        self.plot=P.get('plot')
        self.SaveStats=P.get('save stats')
        self.mutate_order=P.get('mutate order')
        self.rank=rank
        self.FT_in = FT_in
        self.FT_out = FT_out
        self.FT_prec=time_module.time(0,0,0,0)
        self.FT_next=time_module.time(0,0,0,0)
        self.mutate_order_gene=P.get("mutate order each n generations")

        self.Graph =  graph

        
        # self.Graph = local_graph
        self.NbNodes = self.Graph.number_of_nodes()

    def init_inidividual(self):
        proc_list=[rd.randint(1, self.NbProc) for _ in range(self.Graph.number_of_nodes())]
        cyclic_reduction(proc_list,self.NbProc)
        return np.array([RandomTopologicalOrder(self.Graph), proc_list])

    def crossover_map(self, ind1, ind2):
        n = len(ind1[0])
        crossover_point=rd.randint(0,n-1)
        save=np.copy(ind1[1,crossover_point:])
        ind1[1,crossover_point:]=np.copy(ind2[1,crossover_point:])
        ind2[1,crossover_point:]=save
        cyclic_reduction(ind1[1],self.NbProc)
        cyclic_reduction(ind2[1],self.NbProc)

    def crossover_cpga(self, ind1, ind2):  # Conserve dans chaque individu son début
        n = len(ind1[0])
        crossover_point = rd.randint(0, n - 1)
        ind1_original, ind2_original = np.copy(ind1), np.copy(ind2)
        ind1_c, ind2_c = np.copy(ind1[:, crossover_point:]), np.copy(ind2[:, crossover_point:])

        ind1_c_0 = ind1_c[0, :]
        ind1_c_0.flatten()
        ind2_c_0 = ind2_c[0, :]
        ind2_c_0.flatten()

        ind1_0 = np.copy(ind1[0, :])
        ind1_0.flatten()
        ind2_0 = np.copy(ind2[0, :])
        ind2_0.flatten()

        l_index_2, l_index_1 = [], []
        for i, j in zip(ind1_c_0, ind2_c_0):
            l_index_2.append(np.where(ind2_0 == i)[0][0])
            l_index_1.append(np.where(ind1_0 == j)[0][0])
        l_index_1.sort()
        l_index_2.sort()

        ind1[:, crossover_point:] = ind2_original[:, l_index_2]
        ind2[:, crossover_point:] = ind1_original[:, l_index_1]

    def mutation_cpga(self,ind1,size_window,prob,toolbox): #New mutation used for nudging the topological order
        if rd.random() < prob:
            ind2=toolbox.Individual()
            ind2[1,:]=ind1[1,:]
            n = len(ind1[0])
            crossover_point = rd.randint(0, n - 1-size_window)
            crossover_point_2 = crossover_point+size_window

            ind2_original = np.copy(ind2)
            ind1_c, ind2_c = np.copy(ind1[:, crossover_point:crossover_point_2]), np.copy(ind2[:, crossover_point:crossover_point_2])

            ind1_c_0 = ind1_c[0, :]
            ind1_c_0.flatten()
            ind2_c_0 = ind2_c[0, :]
            ind2_c_0.flatten()

            ind2_0 = np.copy(ind2[0, :])
            ind2_0.flatten()

            l_index_2= []
            for i in ind1_c_0:
                l_index_2.append(np.where(ind2_0 == i)[0][0])
            l_index_2.sort()

            ind1[:, crossover_point:crossover_point_2] = ind2_original[:, l_index_2]
            sys.stdout.flush()
            del ind1.fitness.values
        return toolbox.clone(ind1)

    def mutation(self, arg):
        ind, prob = arg
        n = len(ind[0])
        if rd.random() < prob:
            for i in range(n):
                if rd.random() < 0.05:
                    random_processor = rd.randint(1, self.NbProc)
                    ind[1][i] = random_processor
                    del ind.fitness.values
            cyclic_reduction(ind[1],self.NbProc)

    def evaluate(self, individual):
        # implement the schedule length function as seen in page 4 of the paper
        #m Nb of processors
        RT = [time_module.time(0,0,0,0) for _ in range(self.NbProc)]
        processors = list(individual[1]).copy()
        LT = list(individual[0]).copy()

        ST = {}
        FT = {}
        empty_slots={i:{"nb_empty":0} for i in range(self.NbProc)}

        for i in range(self.NbNodes):
            task = LT.pop(0)
            Pj = int(processors[i])
            predecessors = list(self.Graph.predecessors(task))
            DAT = time_module.time(0, 0, 0, 0)

            for dependency in predecessors:
                if FT[str(dependency)] >= DAT:
                    DAT = FT[str(dependency)].copy()
            #print("DAT", DAT)
            #print("RT", Pj -1)
            if DAT >= RT[Pj - 1]:
                ST[task] = DAT.copy()
                #We just created a empty slot between RT[Pj-1] and DAT
                nb_empty=empty_slots[Pj-1]['nb_empty']
                empty_slots[Pj-1][nb_empty]={"length":DAT.to_float()-RT[Pj-1].to_float(),"ST":RT[Pj-1].copy(),"FT":DAT.copy()}
                empty_slots[Pj-1]['nb_empty']=nb_empty+1
                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                RT[Pj - 1] = FT[task].copy()
            else:
                #We might want to put the starting point of this task in an empty slot if at all possible
                empty_filled=False
                time_task=self.Graph.nodes[task]["time"].to_float()
                for i in list(empty_slots[Pj-1])[1:]:
                    if empty_filled==False:
                        if empty_slots[Pj-1][i]["ST"] > DAT:
                            if empty_slots[Pj-1][i]["length"]>time_task:
                                ST[task]=empty_slots[Pj-1][i]["ST"]
                                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                                
                                #change the slot :
                                if empty_slots[Pj-1][i]["FT"]> FT[task]:
                                    empty_slots[Pj-1][i]["ST"]=FT[task].copy()
                                    empty_slots[Pj-1][i]["length"]=empty_slots[Pj-1][i]["FT"].to_float()-empty_slots[Pj-1][i]["ST"].to_float()
                                else:
                                    #delete the slot
                                    empty_slots[Pj-1].pop(i)

                                empty_filled=True
                if empty_filled==False:
                    ST[task] = RT[Pj - 1].copy()
                    FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                    RT[Pj - 1] = FT[task].copy()
        Final_time = time_module.time(0, 0, 0, 0)
        for i in FT.keys():
            if FT[i] >= Final_time:
                Final_time = FT[i]
        Final_time.add(self.FT_prec)
        #print("FT",Final_time)
        #print("REPE", Final_time.to_float())
        return (Final_time.to_float(),)
    
    def evaluate_and_save(self,individual):
        # implement the schedule length function as seen in page 4 of the paper
        #m Nb of processors
        #We add some parameters to also get the worst time possible (time on one processor) by adding all the times
        #And another to get the best time possible, that is to say with no affectation on processors


        timetable={i+1:{"n_tasks":0} for i in range(self.NbProc)}

        RT = [self.FT_prec.copy() for _ in range(self.NbProc)]
        processors = list(individual[1]).copy()
        LT = list(individual[0]).copy()
        
        ST = {}
        ST_inf={}

        FT = {}
        FT_inf={}

        empty_slots={i:{"nb_empty":0} for i in range(self.NbProc)}


        worst_time=time_module.time(0,0,0,0)

        for i in range(self.NbNodes):
            task = LT.pop(0)
            Pj = int(processors[i])
            predecessors = list(self.Graph.predecessors(task))
            DAT = self.FT_prec.copy() #time_module.time(0, 0, 0, 0)
            DAT_inf= self.FT_prec.copy() # time_module.time(0,0,0,0)
            for dependency in predecessors:
                if FT[str(dependency)] >= DAT:
                    DAT = FT[str(dependency)].copy()

                if FT_inf[str(dependency)] >= DAT:
                    DAT_inf = FT[str(dependency)].copy()
            #print("DAT", DAT)
            #print("RT", Pj -1)
            if DAT >= RT[Pj - 1]:
                ST[task] = DAT.copy()
                
                #We just created a empty slot between RT[Pj-1] and DAT
                nb_empty=empty_slots[Pj-1]['nb_empty']
                empty_slots[Pj-1][nb_empty]={"length":DAT.to_float()-RT[Pj-1].to_float(),"ST":RT[Pj-1].copy(),"FT":DAT.copy()}
                empty_slots[Pj-1]['nb_empty']=nb_empty+1
                
                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                RT[Pj - 1] = FT[task].copy()

            else:
                #We might want to put the starting point of this task in an empty slot if at all possible
                empty_filled=False
                time_task=self.Graph.nodes[task]["time"].to_float()
                for i in list(empty_slots[Pj-1])[1:]:
                    if empty_filled==False:
                        if empty_slots[Pj-1][i]["ST"] > DAT:
                            if empty_slots[Pj-1][i]["length"]>time_task:
                                ST[task]=empty_slots[Pj-1][i]["ST"]
                                FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                                
                                #change the slot :
                                if empty_slots[Pj-1][i]["FT"]> FT[task]:
                                    empty_slots[Pj-1][i]["ST"]=FT[task].copy()
                                    empty_slots[Pj-1][i]["length"]=empty_slots[Pj-1][i]["FT"].to_float()-empty_slots[Pj-1][i]["ST"].to_float()
                                else:
                                    #delete the slot
                                    empty_slots[Pj-1].pop(i)

                                empty_filled=True
                if empty_filled==False:
                    ST[task] = RT[Pj - 1].copy()
                    FT[task] = time_module.add(ST[task], self.Graph.nodes[task]["time"])
                    RT[Pj - 1] = FT[task].copy()

            ST_inf[task]=DAT_inf.copy()
            FT_inf[task]=time_module.add(ST_inf[task], self.Graph.nodes[task]["time"])

            worst_time.add(self.Graph.nodes[task]["time"])            

            #we start the task "task" on processor Pj starting at ST and finishing at FT
            nb_tasks=timetable[Pj]["n_tasks"]
            timetable[Pj][nb_tasks+1]={"node":task,"ST":ST[task].to_float(),"FT":FT[task].to_float()}
            timetable[Pj]["n_tasks"]= nb_tasks+1

        Final_time = time_module.time(0, 0, 0, 0)
        Final_time_inf=time_module.time(0,0,0,0)
        for i in FT.keys():
            if FT[i] >= Final_time:
                Final_time = FT[i]
            if FT_inf[i] >= Final_time_inf:
                Final_time_inf = FT_inf[i]
        # print("FT",Final_time)
        # print("FTINF",Final_time_inf)
        #print("REPE", Final_time.to_float())
        self.FT_next=Final_time.copy()
        Final_time.add(self.FT_prec)
        return (Final_time,timetable,worst_time,Final_time_inf)        

    def preselect_all(self,pop,toolbox, select_size): #Utilisé sur toute la population dans rank 0
        offspring = toolbox.select(pop,select_size)
        offspring = [toolbox.clone(x) for x in offspring]
        return offspring

    def init_deap(self, toolbox):
        creator.create("Fitness", base.Fitness, weights=(self.weights,))
        creator.create("Individual", np.ndarray, fitness=creator.Fitness)
        self.toolbox = base.Toolbox()
        toolbox.register("Init_ind", self.init_inidividual)
        toolbox.register("Individual", tools.initIterate, creator.Individual,toolbox.Init_ind)
        toolbox.register("Population", tools.initRepeat, list, toolbox.Individual)
        toolbox.register("cmap", self.crossover_map)
        toolbox.register("cpga", self.crossover_cpga)
        toolbox.register("evaluate", self.evaluate)
        toolbox.register("evaluate_and_save", self.evaluate_and_save)
        toolbox.register("select", tools.selTournament, tournsize=8) #8 because approximately self.PopSize/10
        toolbox.register("mutate_order",self.mutation_cpga)
        toolbox.register("mutate", self.mutation)

    def eval_pop(self,pop,toolbox): 
        costs=[toolbox.evaluate(ind) for ind in pop]

        for ind, cost in zip(pop, costs):
            ind.fitness.values = cost
        if self.verbose:
            # print(f"Evaluated {str(len(pop))} individuals")
            sys.stdout.flush()
        return pop

    def mutate_population_order(self,pop,toolbox):
        for i in range(len(pop)):
            pop[i] = self.mutation_cpga(pop[i],len(data_in[i][0])//10,self.mut_prob,toolbox)

    def update_graph(self,pop,toolbox,FT_changes):
        for ind in pop:
            del ind.fitness.values

        #FT_changes : dictionnaires avec FT_chanes[noeud]= FT de ce noeud
        for in_node in FT_changes:
            if in_node in self.FT_in:
                for (corr_node,corr_rank) in self.FT_in[in_node]:
                    self.Graph.nodes[corr_node]["in_dep"][in_node]['FT']=FT_changes[in_node]
        pass

    def update_gene(self,gene):
        self.nGenerations=gene

    def update_FT(self,pop, toolbox,FT_prec):
        # for ind in pop:
            # del ind.fitness.values
        
        self.FT_prec=FT_prec

    def fit(self, toolbox,u_pop=None,keep_pop=False):
        """
        # Create initial population
        """
        absolute_start_time=time.time()
        
        if not keep_pop:
            pop=toolbox.Population(self.PopSize)
        else:
            pop=u_pop
        costs_proc = [toolbox.evaluate(ind) for ind in pop]
        for ind, cost in zip(pop, costs_proc):
            ind.fitness.values = cost
        costs_proc = [ind.fitness.values[0] for ind in pop]
        
        current_min=None
        min_list=[]
        max_list=[]
        avg_list=[]
        std_list=[]
        #initialisation à 0 pr simplifier le plot avec initialisation
        sel_fill_time=[0]
        cross_over_time=[0]
        mutation_time=[0]
        calculate_costs_time=[0]

        # print(len(pop))

        g = 0
        
        if self.verbose:
            # print("Initial costs for rank 0, rank {}:", costs_proc,self.rank)
            init_time=time.time()-absolute_start_time
            # print("time for initialisation : {}".format(init_time))
            sys.stdout.flush()
        
        # print(g, self.nGenerations)
        while g < self.nGenerations:
            g += 1
            if self.verbose:
                sys.stdout.flush()
                # print("--Generation {}-- rank : {}".format(g,self.rank))
                start_time = time.time()
            offspring = self.preselect_all(pop, toolbox, self.SelectSize)
            while len(offspring)<self.PopSize:
                offspring.append(toolbox.clone(offspring[rd.randint(0,self.SelectSize-1)]))
            rd.shuffle(offspring)
            
            if self.verbose:
                t1=time.time() - start_time
                sel_fill_time.append(t1)
                # print("selection+fill:" + str(t1))
                sys.stdout.flush()
                start_time = time.time()
            
            # Apply crossover and mutations

            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if rd.random() < self.cross_prob:
                    if rd.random() < self.cpga_prob:
                        toolbox.cpga(child1, child2)
                        del child1.fitness.values
                        del child2.fitness.values
                    else:
                        toolbox.cmap(child1, child2)
                        del child1.fitness.values
                        del child2.fitness.values

            if self.verbose:
                t1=time.time() - start_time
                cross_over_time.append(t1)
                # print("crossover :" + str(t1))
                sys.stdout.flush()
                start_time = time.time()

            #need to update the parameter if the second were to be used
            if g%self.mutate_order_gene <= 4: #or len(list(set(min_list[-100:]))):#No update
                if rd.random()< self.mutate_order:
                    order=1
                else:
                    order=0
            
                if order==1:
                    self.mutate_population_order(offspring,toolbox)
                else:
                    for mutant in offspring:
                        toolbox.mutate((mutant, self.mut_prob))
            else:
                for mutant in offspring:
                    toolbox.mutate((mutant, self.mut_prob))
            
            if self.verbose:
                t1=time.time()-start_time
                mutation_time.append(t1)
                # print("mutation:" + str(t1))
        
            # Update the fitness of invalid costs

            start_time = time.time()
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]

            invalid_ind=self.eval_pop(invalid_ind,toolbox)


            if self.verbose:
                t1=time.time()-start_time
                calculate_costs_time.append(t1)
                # print("calculate_new_costs:" + str(t1))
            
            pop[:] = offspring

            # Gather all the costs in one list and print the stats
            costs=[]
            for ind in pop:
                current_fit=ind.fitness.values[0]
                if current_min == None or current_fit <= current_min:
                    best_ind=ind
                    current_min=current_fit
                costs.append(current_fit)
            

            length = len(pop)
            mean = sum(costs) / length
            sum2 = sum(x * x for x in costs)
            std = abs(sum2 / length - mean ** 2) ** 0.5

            min_list.append(min(costs))
            max_list.append(max(costs))
            avg_list.append(mean)
            std_list.append(std)
            it=[init_time] + [0 for i in range(self.nGenerations)]

            # if self.verbose:
                # print("  Min {}, {}".format(min(costs), self.rank))
                # print("  Max %s" % max(costs))
                # print("  Avg %s" % mean)
                # print("  Std %s" % std)

            if g==self.nGenerations:
                #time log :
                final_time,timetble,worst_time,time_inf=toolbox.evaluate_and_save(best_ind)
                total_time=time.time()-absolute_start_time
                print("total time : {} s".format(total_time))
                sys.stdout.flush()

                return max_list,pop,self.FT_next,min_list,avg_list


def cyclic_reduction(ind,nproc):
    #get a unique representation by forcing the first processor to one
    #and each new unique change to the next processor
    proc_count=1
    indice_proc=0
    renumber={}
    while proc_count <= nproc and indice_proc<=len(ind)-1:
        if ind[indice_proc] not in renumber:
            renumber[ind[indice_proc]]=str(proc_count)
            proc_count +=1
        indice_proc+=1
    
    #renumber
    for i in range(len(ind)):
        ind[i] = renumber[ind[i]]
    