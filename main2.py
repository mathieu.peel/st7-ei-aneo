import random as rd
import json
import numpy as np
import process_DAG
import time_module
import multiprocessing as mp
import time
import argparse
import sys
from pathlib import Path

from deap import base
from deap import creator
from deap import tools
from Analyser import Parser
from deap_chromosome import *


class Flags:
    regressor = None
    euro = False
    verboseMode = False
    seed = None
    loops = 1
    infile = None
    indir = None
    fdout = None
    processes = None

    def __init__(self, argparser):
        args = argparser.parse_args()
        if not args.infile and not args.indir:
            print('Missing parameter --infile or --indir')
            parser.print_help()
            sys.exit(1)
        elif args.infile:
            self.infile = args.infile
        elif args.indir:
            self.indir = args.indir
        if args.verbose:
            self.verboseMode = True
        if args.loops:
            self.loops = args.loops
        if args.processes:
            self.processes = args.processes
        if args.seed:
            self.seed = args.seed
        if args.outfile:
            self.fdout = open(args.outfile, 'w')

class ArgsWrapper:
    def __init__(self, infile, iLoop, verbose):
        self.infile = infile
        self.iLoop = iLoop
        #self.rng = rng
        self.verbose = verbose

# We must create this variables globally
creator.create("Fitness", base.Fitness, weights=(-1,))
creator.create("Individual", np.ndarray, fitness=creator.Fitness)


def runSingleGenetic(args: ArgsWrapper):
    start_time0 = time.time()
    p = Parser(args.infile)
    #Model.new(p, args.rng)
    Gene = Genetic(p, args.verbose)
    if args.verbose and MPI_RANK==0:
        print("verbose mode = true")
        sys.stdout.flush()
    toolbox = base.Toolbox()
    Gene.init_deap(toolbox)
    Gene.fit(toolbox)
    computationalTime = time.time() - start_time0
    return (1, 1, computationalTime)


def runGenetic(infile, flags: Flags):
    p = Parser(infile)
    mean_lowerprice = 0
    mean_price = 0
    stddev_price = 0
    time_price = 0
    #rng = np.random.MT19937(flags.seed)
    #with mp.Pool(processes=flags.processes) as workers:
        #workersResults = workers.imap_unordered(runSingleGenetic, [ArgsWrapper(infile, i, flags.verboseMode) for i in range(0, flags.loops)])
        # results = workers.starmap(runSingleAmerMonteCarlo, [(infile, i, rng.jumped(i), flags) for i in range(0, flags.loops)])
    for i in range(0, flags.loops):
            #lowerprice_i, price_i, time_i = workersResults.next()
        lowerprice_i, price_i, time_i = runSingleGenetic(ArgsWrapper(infile, i, flags.verboseMode))
            #teePrint(flags.fdout, f'Iteration {i} American price: {price_i}')
            #teePrint(flags.fdout, f'Iteration {i} American price: {lowerprice_i}')
        mean_price += price_i
        mean_lowerprice += lowerprice_i
        stddev_price += price_i * price_i
        time_price += time_i
    mean_price /= flags.loops
    mean_lowerprice /= flags.loops
    stddev_price = np.sqrt(stddev_price / flags.loops - mean_price * mean_price)
    time_price /= flags.loops

    #teePrint(flags.fdout, f'Average American price: {mean_price}\tStddev: {stddev_price}')
    #teePrint(flags.fdout, f'Average lower American price: {mean_lowerprice}')
    #teePrint(flags.fdout, f'Computational time: {time_price}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    group1 = parser.add_mutually_exclusive_group(required=True)
    group1.add_argument("--infile", help="Path to the files describing the problems. One file per problem", nargs='+', type=str)
    group1.add_argument("--indir", help="Run all problems inside the directory.", type=str)
    parser.add_argument("--outfile", "-o", help="Path to the output file", type=str)
    parser.add_argument("--verbose", help="Verbose", action="store_true")
    parser.add_argument("--seed", help="Rng seed", type=int)
    parser.add_argument("--loops", help="Number of loops", type=int)
    parser.add_argument("--processes", help="Number of processes to run in parallel when using --loops", type=int)

    argflags = Flags(parser)
    Files = []
    if argflags.infile is not None:
        Files = [f for f in argflags.infile if f.endswith('.txt') and not f.endswith('_results.txt')]

    if argflags.indir is not None:
        Files = [f for f in Path(argflags.indir).iterdir() if f.match('*.txt') and not f.match('*_results.txt')  and not f.endswith('.template.txt')]

    nFiles = len(Files)
    for idx, f in enumerate(Files):
        #teePrint(argflags.fdout, f'-- Input file: {f}\t({idx + 1}/{nFiles})')
        runGenetic(f, argflags)

    if argflags.fdout is not None:
        argflags.fdout.close()